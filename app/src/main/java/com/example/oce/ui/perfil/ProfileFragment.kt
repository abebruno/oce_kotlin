package com.example.oce.ui.perfil


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.oce.MainActivity

import com.example.oce.R
import com.example.oce.ui.perfil.component.DaggerProfileComponent
import com.example.oce.ui.perfil.module.ProfileModule
import com.example.oce.ui.perfil.recyclerView.RecyclerViewProfileItem
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.fragment_profile.view.*
import javax.inject.Inject

class ProfileFragment : Fragment(), ProfileView {
    @Inject
    lateinit var profilePresenter: ProfilePresenter

    lateinit var recyclerViewProfile: RecyclerView

    lateinit var mainActivity: MainActivity

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_profile, container, false)
        val profileComponent = DaggerProfileComponent
            .builder()
            .profileModule(ProfileModule(this))
            .build()

        profileComponent.inject(this)
        this.mainActivity = activity as MainActivity

        this.recyclerViewProfile = rootView.recyclerViewProfile
        profilePresenter.getFakeData()
        return rootView
    }

    override fun showFakeProfileDatas() {
        this.recyclerViewProfile.layoutManager = LinearLayoutManager(context)
        val adapter = GroupAdapter<ViewHolder>()

        for(i in 0 until 20){
            adapter.add(RecyclerViewProfileItem())
        }

        this.recyclerViewProfile.adapter = adapter
    }
}
