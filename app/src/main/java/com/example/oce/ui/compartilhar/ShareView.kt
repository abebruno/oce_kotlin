package com.example.oce.ui.compartilhar

interface ShareView {
    fun navigateToEnchente()
    fun navigateToIntensidadeChuva()
    fun navigateToLeitoRio()
    fun navigateToFoto()
}