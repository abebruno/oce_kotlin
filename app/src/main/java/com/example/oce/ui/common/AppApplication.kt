package com.example.oce.ui.common

import android.app.Application
import com.example.oce.ui.common.component.AppComponent
import com.example.oce.ui.common.component.DaggerAppComponent
import com.example.oce.ui.common.module.AppModule
import dagger.Component

class AppApplication: Application() {
    companion object{
        lateinit var applicationComponent: AppComponent
        lateinit var app: Application
    }

    override fun onCreate() {
        super.onCreate()

        app = this
        applicationComponent = DaggerAppComponent
            .builder()
            .appModule(AppModule())
            .build()
    }
}