package com.example.oce.ui.common.module

import com.example.oce.data.remote.RemoteDataSource
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@Module
class AppModule {
    @Provides
    fun getRetrofit(): Retrofit{
        val baseUrl = "https://desafio-mobile.nyc3.digitaloceanspaces.com/"
        return Retrofit
            .Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
    }

    @Provides
    fun getRemoteDataSource(retrofit: Retrofit): RemoteDataSource{
        return retrofit.create(RemoteDataSource::class.java)
    }
}