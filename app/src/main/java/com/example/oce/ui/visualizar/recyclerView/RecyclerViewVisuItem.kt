package com.example.oce.ui.visualizar.recyclerView

import com.example.oce.R
import com.example.oce.data.model.User
import com.example.oce.utils.GlideApp
import com.xwray.groupie.kotlinandroidextensions.Item
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import kotlinx.android.synthetic.main.recycler_view_visu_layout.view.*

class RecyclerViewVisuItem(val user: User): Item() {
    override fun bind(viewHolder: ViewHolder, position: Int) {
        val item = viewHolder.itemView

        item.textViewVisuDate.text = "26/02/1997"
        item.textViewVisuName.text = user.name
        item.textViewVisuRelato.text = user.name
        GlideApp.with(item.context).load(user.imag).error(R.drawable.no_image).into(item.imageView)
    }

    override fun getLayout(): Int {
        return R.layout.recycler_view_visu_layout
    }
}