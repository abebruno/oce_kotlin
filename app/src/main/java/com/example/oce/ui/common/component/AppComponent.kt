package com.example.oce.ui.common.component

import com.example.oce.data.remote.RemoteDataSource
import com.example.oce.ui.common.module.AppModule
import dagger.Component

@Component(modules = [AppModule::class])
interface AppComponent {
    fun getRemoteDataSource(): RemoteDataSource
}