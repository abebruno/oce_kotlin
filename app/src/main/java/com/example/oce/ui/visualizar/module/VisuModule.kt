package com.example.oce.ui.visualizar.module

import com.example.oce.ui.visualizar.VisuView
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable

@Module
class VisuModule(private val visuView: VisuView){

    @Provides
    fun getVisuView(): VisuView{
        return this.visuView
    }

    @Provides
    fun createCompositeDisposable(): CompositeDisposable{
        return CompositeDisposable()
    }
}