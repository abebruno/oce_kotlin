package com.example.oce.ui.perfil.component

import com.example.oce.ui.perfil.ProfileFragment
import com.example.oce.ui.perfil.module.ProfileModule
import dagger.Component

@Component(modules = [ProfileModule::class])
interface ProfileComponent {
    fun inject(profileFragment: ProfileFragment)
}