package com.example.oce.ui.perfil.module

import com.example.oce.ui.perfil.ProfileView
import dagger.Module
import dagger.Provides

@Module
class ProfileModule(private val profileView: ProfileView){
    @Provides
    fun getProfileView():ProfileView{
        return this.profileView
    }
}