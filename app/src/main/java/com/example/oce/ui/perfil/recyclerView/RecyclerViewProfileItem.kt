package com.example.oce.ui.perfil.recyclerView

import com.example.oce.R
import com.xwray.groupie.kotlinandroidextensions.Item
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import kotlinx.android.synthetic.main.recycler_view_visu_layout.view.*

class RecyclerViewProfileItem: Item() {
    override fun bind(viewHolder: ViewHolder, position: Int) {
        val item = viewHolder.itemView

        item.textViewVisuDate.text = "UHUUULLL"
        item.textViewVisuName.text = "UHEEELLL"
        item.textViewVisuRelato.text = "ABE"
    }

    override fun getLayout(): Int {
        return R.layout.recycler_view_visu_layout
    }
}