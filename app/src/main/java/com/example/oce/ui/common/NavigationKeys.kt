package com.example.oce.ui.common

import androidx.fragment.app.Fragment
import com.example.oce.ui.compartilhar.ShareFragment
import com.example.oce.ui.compartilhar.enchente_rua.EnchenteRuasFragment
import com.example.oce.ui.compartilhar.foto.FotoFragment
import com.example.oce.ui.compartilhar.intensidade_chuva.IntensidadeChuvaFragment
import com.example.oce.ui.compartilhar.leito_rio.LeitoRioFragment
import com.example.oce.ui.perfil.ProfileFragment
import com.example.oce.ui.visualizar.VisuFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen

class NavigationKeys {
    class Visu: SupportAppScreen(){
        override fun getFragment(): Fragment {
            return VisuFragment()
        }
    }

    class Share: SupportAppScreen(){
        override fun getFragment(): Fragment{
            return ShareFragment()
        }
    }

    class Profile: SupportAppScreen(){
        override fun getFragment(): Fragment{
            return ProfileFragment()
        }
    }

    class Foto: SupportAppScreen(){
        override fun getFragment(): Fragment{
            return FotoFragment()
        }
    }

    class IntensidadeChuva: SupportAppScreen(){
        override fun getFragment(): Fragment{
            return IntensidadeChuvaFragment()
        }
    }

    class LeitoRio: SupportAppScreen(){
        override fun getFragment(): Fragment{
            return LeitoRioFragment()
        }
    }

    class EnchenteRua: SupportAppScreen(){
        override fun getFragment(): Fragment{
            return EnchenteRuasFragment()
        }
    }
}