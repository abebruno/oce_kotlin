package com.example.oce.ui.visualizar

import com.example.oce.data.model.User

interface VisuView {
    fun showRelatos(listUsers: List<User>)
}