package com.example.oce.ui.perfil

import javax.inject.Inject

class ProfilePresenter @Inject constructor(private val profileView: ProfileView){
    fun getFakeData(){
        profileView.showFakeProfileDatas()
    }
}