package com.example.oce.ui.visualizar

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.oce.MainActivity

import com.example.oce.R
import com.example.oce.data.model.User
import com.example.oce.data.remote.RemoteDataSource
import com.example.oce.ui.common.AppApplication
import com.example.oce.ui.common.component.AppComponent
import com.example.oce.ui.visualizar.component.DaggerVisuComponent
import com.example.oce.ui.visualizar.module.VisuModule
import com.example.oce.ui.visualizar.recyclerView.RecyclerViewVisuItem
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_visu.*
import kotlinx.android.synthetic.main.fragment_visu.view.*
import javax.inject.Inject

class VisuFragment : Fragment(), VisuView {

    @Inject
    lateinit var visuPresenter: VisuPresenter

    lateinit var recyclerViewVisu: RecyclerView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_visu, container, false)
        val componentVisu = DaggerVisuComponent
            .builder()
            .appComponent(AppApplication.applicationComponent)
            .visuModule(VisuModule(this))
            .build()

        componentVisu.inject(this)

        this.recyclerViewVisu = rootView.recyclerViewVisu
        this.visuPresenter.getDataLocal()

        return rootView
    }

    override fun showRelatos(listUsers: List<User>) {
        this.recyclerViewVisu.layoutManager = LinearLayoutManager(context)
        val adapter = GroupAdapter<ViewHolder>()

        listUsers.forEach {
            adapter.add(RecyclerViewVisuItem(it))
        }

        this.recyclerViewVisu.adapter = adapter
    }

    override fun onPause() {
        super.onPause()
        this.visuPresenter.clearCompositeDisposable()
    }

    override fun onDestroy() {
        super.onDestroy()
        this.visuPresenter.disposeCompositeDisposable()
    }

}
