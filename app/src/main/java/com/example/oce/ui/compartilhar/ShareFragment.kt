package com.example.oce.ui.compartilhar


import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import com.example.oce.MainActivity

import com.example.oce.R
import com.example.oce.ui.common.NavigationKeys
import com.google.android.material.bottomnavigation.BottomNavigationMenu
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.jakewharton.rxbinding3.view.clicks
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.view.*
import kotlinx.android.synthetic.main.fragment_share.*
import kotlinx.android.synthetic.main.fragment_share.view.*

class ShareFragment : Fragment(), ShareView {

    lateinit var mainActivity: MainActivity

    @SuppressLint("CheckResult")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_share, container, false)
        this.mainActivity = activity as MainActivity

        rootView.button_altura_enchente.setOnClickListener{ navigateToEnchente() }

        return rootView
    }

    override fun navigateToEnchente() {
        this.mainActivity.cicerone.router.navigateTo(NavigationKeys.EnchenteRua())
    }

    override fun navigateToIntensidadeChuva() {
        this.mainActivity.cicerone.router.navigateTo(NavigationKeys.IntensidadeChuva())
    }

    override fun navigateToLeitoRio() {
        this.mainActivity.cicerone.router.navigateTo(NavigationKeys.LeitoRio())
    }

    override fun navigateToFoto() {
            this.mainActivity.cicerone.router.navigateTo(NavigationKeys.Foto())
    }
}
