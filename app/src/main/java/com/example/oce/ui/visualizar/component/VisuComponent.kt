package com.example.oce.ui.visualizar.component

import com.example.oce.ui.common.component.AppComponent
import com.example.oce.ui.visualizar.VisuFragment
import com.example.oce.ui.visualizar.module.VisuModule
import dagger.Component
import io.reactivex.disposables.CompositeDisposable

@Component(modules = [VisuModule::class], dependencies = [AppComponent::class])
interface VisuComponent {
    fun inject(visuFragment: VisuFragment)
    fun createCompositeDisposable(): CompositeDisposable
}