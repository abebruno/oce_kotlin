package com.example.oce.ui.visualizar

import android.util.Log
import com.example.oce.data.remote.RemoteDataSource
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class VisuPresenter @Inject constructor(private val visuView: VisuView, private val remoteDataSource: RemoteDataSource, private val compositeDisposable: CompositeDisposable){
    fun getDataLocal(){
        val single = remoteDataSource
            .getData()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe (
                {
                    Log.d("HelpMe", it.toString())
                    visuView.showRelatos(it)
                },
                {
                    Log.d("HelpMe", it.toString())
                }
            )

        compositeDisposable.add(single)
    }

    fun clearCompositeDisposable(){
        this.compositeDisposable.clear()
    }

    fun disposeCompositeDisposable(){
        this.compositeDisposable.dispose()
    }
}