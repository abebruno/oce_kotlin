package com.example.oce

import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.android.support.SupportAppNavigator

@Module
class MainModule(private val mainActivity: MainActivity) {
    @Provides
    fun getNavigatot(): Navigator{
        return SupportAppNavigator(mainActivity, R.id.fragmentsContainer)
    }

    @Provides
    fun createCicerone(): Cicerone<Router>{
        return Cicerone.create()
    }

    @Provides
    fun getRouter(cicerone: Cicerone<Router>): Router{
        return cicerone.router
    }

    @Provides
    fun getNavigator(cicerone: Cicerone<Router>): NavigatorHolder {
        return cicerone.navigatorHolder
    }
}