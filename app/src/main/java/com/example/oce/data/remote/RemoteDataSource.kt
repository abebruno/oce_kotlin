package com.example.oce.data.remote

import com.example.oce.data.model.User
import io.reactivex.Single
import retrofit2.http.GET

interface RemoteDataSource {
    @GET("movies")
    fun getData(): Single<List<User>>
}