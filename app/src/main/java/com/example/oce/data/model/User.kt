package com.example.oce.data.model

import com.google.gson.annotations.SerializedName

data class User(
    val id: Int,

    @SerializedName("title")
    val name: String,

    @SerializedName("poster_url")
    val imag: String
)