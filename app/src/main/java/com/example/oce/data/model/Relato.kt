package com.example.oce.data.model

data class Relato(
    val id: Int,
    val relato: String,
    val date: String,
    val owner: User
)