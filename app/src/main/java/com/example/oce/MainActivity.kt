package com.example.oce

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import com.example.oce.R
import com.example.oce.ui.common.NavigationKeys
import kotlinx.android.synthetic.main.activity_main.*
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.commands.Command
import ru.terrakok.cicerone.commands.Replace
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var cicerone: Cicerone<Router>

    @Inject
    lateinit var navigator: Navigator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val component = DaggerMainComponent
            .builder()
            .mainModule(MainModule(this))
            .build()

        component.inject(this)

        if(savedInstanceState == null){
            navigator.applyCommands(arrayOf<Command>(Replace(NavigationKeys.Visu())))
        }

        bottomNavigation.setOnNavigationItemSelectedListener{ onNavigationItemSelected(it) }
    }

    private fun onNavigationItemSelected(item: MenuItem): Boolean{
        when(item.itemId){
            R.id.navigation_visualization -> {
                this.cicerone.router.navigateTo(NavigationKeys.Visu())
                return true
            }
            R.id.navigation_share -> {
                this.cicerone.router.navigateTo(NavigationKeys.Share())
                return true
            }
            R.id.navigation_profile -> {
                this.cicerone.router.navigateTo(NavigationKeys.Profile())
                return true
            }
        }
        return true
    }

    override fun onResume() {
        super.onResume()
        this.cicerone.navigatorHolder.setNavigator(this.navigator)
    }

    override fun onPause() {
        super.onPause()
        this.cicerone.navigatorHolder.removeNavigator()
    }

    override fun onDestroy() {
        super.onDestroy()
        this.cicerone.router.exit()
    }
}
