package com.example.oce

import dagger.Component

@Component(modules = [MainModule::class])

interface MainComponent {
    fun inject(mainActivity: MainActivity)
}